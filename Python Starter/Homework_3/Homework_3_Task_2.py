"""Завдання 2. Напишіть програму, яка обчислює значення функції
𝑦 = cos(3x), де - 𝜋 <= x <= 𝜋 та вводиться з клавіатури. Відповідь
подати у радіанах.

"""

import math


value_x = float(
    input("Enter the number 'x' in the range from -𝜋 to 𝜋 inclusive: ")
)

if -math.pi <= value_x <= math.pi:
    print(f"Result in radians: {math.cos(3 * value_x)}.")

else:
    print("The number entered is not within the specified range.")
