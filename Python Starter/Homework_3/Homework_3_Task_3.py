"""Завдання 3. Напишіть програму, яка розв'язує квадратне рівняння
𝑎𝑥 2 + 𝑏𝑥 + 𝑐 = 0 у дійсних числах. На відміну від аналогічної вправи
з минулого уроку, програма повинна видавати повідомлення про
відсутність дійсних коренів, якщо значення дискримінанта 𝐷 = 𝑏 2 −4𝑎𝑐
негативне, єдине рішення 𝑥 = − 𝑏 / 2𝑎, якщо він дорівнює нулю, або
два корені 𝑥1,2 = −b±√D/2a, якщо він позитивний.

"""

value_a = float(input("Enter 'a' = "))
value_b = float(input("Enter 'b' = "))
value_c = float(input("Enter 'c' = "))

discriminant = value_b ** 2 - 4 * value_a * value_c
print(f"Discriminant = {discriminant}")

if discriminant < 0:
    print(f"Discriminant negative. No square roots.")

elif discriminant > 0:
    print(f"Discriminant positive. Two square roots. "
          f"Solved by formulas: x1,2 = −b±√D/2a.")

elif discriminant == 0:  # прописано "elif" замість "else" для повного відображення всіх умов
    print(f"Discriminant is zero. One square root. "
          f"Is solved by the formula: x = −b/2a.")
