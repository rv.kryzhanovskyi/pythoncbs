"""Завдання 4. Напишіть програму-калькулятор, в якій користувач зможе
обрати операцію, ввести необхідні числа та отримати результат.
Операції, які необхідно реалізувати: додавання, віднімання, множення,
ділення, зведення в ступінь, квадратний корінь, кубічний корінь, синус,
косинус та тангенс числа.

"""

import math


mat_operation = input(
        "\nEnter a mathematical operation:\n\t"
        "addition\n\t"
        "subtraction\n\t"
        "multiplication\n\t"
        "division\n\t"
        "power\n\t"
        "square_root\n\t"
        "cube_root\n\t"
        "sin\n\t"
        "cos\n\t"
        "tan\n\t"
        "-->"
).lower()

if mat_operation in (
        "addition",
        "subtraction",
        "multiplication",
        "division"
):

    first_number = float(input("Enter first number: "))
    second_number = float(input("Enter second number: "))

    if mat_operation == "addition":
        print(f"Calculation result: {first_number + second_number}")

    elif mat_operation == "subtraction":
        print(f"Calculation result: {first_number - second_number}")

    elif mat_operation == "multiplication":
        print(f"Calculation result: {first_number * second_number}")

    elif mat_operation == "division":

        if second_number == 0:
            print("Error: division by zero!")

        else:
            print(f"Calculation result: {first_number / second_number}")

elif mat_operation in (
        "power",
        "square_root",
        "cube_root",
        "sin",
        "cos",
        "tan"
):
    number = float(input("Enter number: "))

    if mat_operation == "power":
        power = int(input("Enter the power as an integer: "))
        print(f"Calculation result: {pow(number, power)}")

    elif mat_operation == "square_root":

        if number < 0:
            print(f"Error: "
                  f"The square root of a negative number is not calculated!")

        else:
            print(f"Calculation result: {math.sqrt(number)}")

    elif mat_operation == "cube_root":

        if number < 0:
            print(f"Calculation result: {-abs(number) ** (1 / 3)}")

        else:
            print(f"Calculation result: {number ** (1 / 3)}")

    elif mat_operation == "sin":
        print(f"Calculation result in radians: {math.sin(number)}")

    elif mat_operation == "cos":
        print(f"Calculation result in radians: {math.cos(number)}")

    elif mat_operation == "tan":
        print(f"Calculation result in radians: {math.tan(number)}")

else:
    print("Error: incorrect math operation name!")
