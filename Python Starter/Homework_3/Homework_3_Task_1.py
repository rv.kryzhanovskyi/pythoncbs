"""Завдання 1. Напишіть програму, яка запитує у користувача його
ім'я, якщо воно збігається з вашим, видає певне повідомлення.

"""

user_name = input("What is your name? ").capitalize()

name_template = "Roman"

if user_name == name_template:
    print(f"Hello, {user_name}. We have the same names!")

else:
    print(f"Hello, {user_name}. My name is {name_template}.")
