"""Завдання 6. Написати програму визначення днів тижня. Дані про день
тижня вводяться користувачем з клавіатури. Якщо будній день -
виводити на екран повідомлення "Сьогодні на роботу", у вихідні дні
- "Сьогодні вихідний", в інших випадках - "Такого дня не існує".

"""

day_week = input(
    "\nSelect what day of the week it is today:\n\t"
    "monday\n\t"
    "tuesday\n\t"
    "wednesday\n\t"
    "thursday\n\t"
    "friday\n\t"
    "saturday\n\t"
    "sunday\n\t"
    "-->"
).lower()

if day_week in ("monday", "tuesday", "wednesday", "thursday", "friday"):
    print("Going to work today.")

elif day_week in ("saturday", "sunday"):
    print("Today is a day off.")

else:
    print("There is no such day.")
