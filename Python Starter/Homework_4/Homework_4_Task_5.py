"""Завдання 5. Створіть програму, яка малює на екрані
прямокутник із зірочок заданою користувачем ширини та висоти.

"""

width_rectangle = int(input("Enter the width of the rectangle: "))
height_rectangle = int(input("Enter the height of the rectangle: "))

for _ in range(height_rectangle):
    for _ in range(width_rectangle):
        print("*", end="")
    print()
