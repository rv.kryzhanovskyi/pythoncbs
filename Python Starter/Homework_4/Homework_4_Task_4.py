"""Завдання 4. Дано числа a і b (a < b). Виведіть на екран суму всіх
натуральних чисел від a до b (включно), які є кратними середньому
арифметичному цього проміжку.

"""

from statistics import mean

number_a = int(input("Enter the smallest integer 'a': "))
number_b = int(input("Enter the largest integer 'b': "))

range_mean = mean(range(number_a, number_b + 1))
sum_number = 0

for i in range(number_a, number_b + 1):
    if i % range_mean == 0:
        sum_number += i

print(
    f"The sum of natural numbers of a range that are multiples of "
    f"the arithmetic mean of this range: {sum_number}"
)
