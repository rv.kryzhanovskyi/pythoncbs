"""Завдання 6. Створіть програму авторизації, в якій користувачеві
дається 3 спроби ввести свої облікові дані (логін та пароль).
Якщо користувач за меншу кількість спроб ввів вірні дані, програма
достроково припиняє своє виконання та виводить на екран
повідомлення: «Авторизацію успішно пройдено з «№» спроби».

"""

user_login = "Roman"
user_password = "123"
count = 0

for i in range(3):
    response_login = input("Enter login: ")
    response_password = input("Enter password: ")
    count += 1

    if user_login == response_login and user_password == response_password:
        print(f"Authorization completed successfully in {count} attempts.")
        break

    else:
        print("Authorization failed, incorrect login or password!")
        print()
else:
    print("You have exhausted 3 attempts!")
