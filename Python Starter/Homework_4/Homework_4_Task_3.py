"""Завдання 3. Використовуючи вкладені цикли та функції print(‘*’, end=’’),
 print(‘ ‘, end=’’) та print() виведіть на екран прямокутний трикутник.

"""
leg = int(input("To display a triangle, enter the length of its leg: "))

for i in range(1, leg + 1):
    for _ in range(i):
        print("*", end="")
        print(" ", end="")
    print()
