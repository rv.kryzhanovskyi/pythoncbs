"""Завдання 2. Факторіалом числа n називається число n!=1∙2∙3∙…∙n.
Створіть програму, яка обчислює факторіал введеного користувачем числа.

"""

number = int(input("To calculate the factorial of a number, enter an integer: "))

result_factorial = 1

for i in range(2, number + 1):
    result_factorial *= i

print(f"{number}! = {result_factorial}")
