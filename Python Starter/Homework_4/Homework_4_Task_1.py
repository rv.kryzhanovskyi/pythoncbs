"""Завдання 1. Дано числа a і b (a < b). Виведіть суму всіх
натуральних чисел від a до b (включно).

"""

number_a = int(input("Enter the smallest integer 'a': "))
number_b = int(input("Enter the largest integer 'b': "))
sum_number = 0

while number_a <= number_b:
    sum_number += number_a
    number_a += 1

print(f"Sum of natural numbers of a given range: {sum_number}")
