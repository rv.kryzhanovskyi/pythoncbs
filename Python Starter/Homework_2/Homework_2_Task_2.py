"""Завдання №2. Напишіть програму, яка запитує три цілі
числа a, b і x та друкує їх добуток.

"""

a = int(input("Enter an integer 'a' = "))
b = int(input("Enter an integer 'b' = "))
x = int(input("Enter an integer 'x' = "))

print(f'{a} * {b} * {x} = {a * b * x}')
