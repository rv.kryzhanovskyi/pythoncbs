"""Завдання №5. Напишіть програму, яка вводить з
клавіатури текст і виводить його в оберненому порядку.

"""

text_string = input("Enter text: ")

print(f'Reverse the order of text characters: {text_string[::-1]}')
