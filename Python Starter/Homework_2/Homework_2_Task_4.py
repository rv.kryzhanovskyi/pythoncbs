"""Завдання №7. Напишіть програму, в якій користувач вводить фразу
з клавіатури, яка складається з 10 символів. На екрані виведіть суму
ASCII-кодів символів введеного рядка.

"""

string_symbols = input("Enter a 10 character string: ")


if len(string_symbols) == 10:
    sum_ascii_value = 0

    for symbol in string_symbols:
        ascii_value = ord(symbol)
        sum_ascii_value += ascii_value

    print(
        f'The sum of the ASCII code values of the symbols in the string '
        f'"{string_symbols}" is equal to {sum_ascii_value}.'
    )

else:
    print("You entered the wrong number of characters.")
