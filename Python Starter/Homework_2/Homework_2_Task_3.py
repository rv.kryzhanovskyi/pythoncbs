""" Завдання №3. Напишіть програму, яка розв'язує квадратне рівняння
𝑎𝑥2 + 𝑏𝑥 + 𝑐 = 0 за формулами 𝑥1,2 = −𝑏± √(b2−4ac)/2𝑎. Значення a, b та c
вводяться з клавіатури. Для знаходження кореня використовуйте оператор
зведення в ступінь, а не функцію math.sqrt, щоб отримати комплексні числа
у випадку, якщо вираз під коренем негативний.

"""

a = float(input("Enter 'a' = "))
b = float(input("Enter 'b' = "))
c = float(input("Enter 'c' = "))

discriminant = b ** 2 - 4 * a * c
print(f'Discriminant = {discriminant}')

if discriminant == 0:
    x = -b / (2 * a)
    print(f'x = {x}')

else:
    x_1 = (-b + discriminant**0.5) / (2 * a)
    x_2 = (-b - discriminant**0.5) / (2 * a)
    print(f'x_1 = {x_1} \nx_2 = {x_2}')
