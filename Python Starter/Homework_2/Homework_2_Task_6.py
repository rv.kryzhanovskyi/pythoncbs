"""Завдання №6. Напишіть програму, яка запитує користувача радіус
круга і виводить його площу. Формула площі круга: S= 𝜋𝑟 2.

"""

import math

radius = float(input("Enter the radius of the circle: "))
area_circle = math.pi * (radius ** 2)

print(f'Area of a circle: {area_circle}')
