"""Завдання №1. Напишіть програму, яка запитує у користувача
два слова і виводить їх розділеними комою.  """

first_word = input("Enter the first word: ")
second_word = input("Enter the second word: ")

print(first_word, second_word, sep=", ")
